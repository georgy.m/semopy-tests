**semopy** testing dataset and scripts for the article.

To repproduce results from the *Numerical experiments* section of the paper, execute "run.sh". This script will do the following:
1. Install required packages as indicated by requirements.txt
2. Generates sets of random models/datasets as indicated in the *Table 3* of the article: runs generate_models.py; they will be stored in "models" folder
3. Estimates parameter estimates via **semopy v2**: runs evaluate_models.py; Results will be stored alongside the generated models
4. Estimates parameter estimates via **semopy v1**: runs evaluate_models__old.py;  Results will be stored alongside the generated models
5. Builds tables with comparisons: runs build_evaluation__results.py; Tables will be stored in "tables" folder.

Each of scripts can be ran independently given there are data produced at the previous step. Alternatively, one can fetch all the generated and evaluated data from the [Google drive repository](https://drive.google.com/file/d/13PUbgaS1qxJFtnla8aI64GFOXiO1uWqx/).
