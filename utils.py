import random
import numpy as np
import os


seed = 2021
random.seed(seed)
np.random.seed(seed)


folder_models = 'models'

folder_accuracy = f'{folder_models}/accuracy'

folder_accuracy_pbs = f'{folder_models}/accuracy_pbs'

folder_efa = f'{folder_models}/efa'

folder_tables = 'tables'

def prepare_path(path: str):
    folders = path.split('/')
    cur_path = str()
    for folder in folders:
        cur_path = os.path.join(cur_path, folder)
        if not os.path.isdir(cur_path):
            os.mkdir(cur_path)
    


