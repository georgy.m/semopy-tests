#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from collections import defaultdict
import pandas as pd
import pickle
import os
from utils import *


pattern =r'''
\begin{tikzpicture}
		\begin{scope}[scale=0.5, transform shape]
		\pgfkeys{{/pgfplots/legend entry/.code=\addlegendentry{#1}}}
		\begin{axis}[
        height=0.5\textheight,
		xlabel={|NAME|},
		ylabel={$N$, number of non-convergent models},
		legend pos=north east,
		ymajorgrids=true,
		grid style=dashed,
        xtick={|TICKS|}
		]
		|ADDPLOTS|
		\end{axis}
		\end{scope}
		\end{tikzpicture}
        '''

ptr = r'''\addplot[style|I|, legend entry={|NAME|}]
		coordinates {
			|COORDS|
		};'''
def create_plot(df, name):
    plots = list()
    for i, n in enumerate(df.index):
        its = ['({},{})'.format(df.columns[j], t.split('&')[0] )
               for j, t in enumerate(df.values[i])]
        its = ''.join(its)
        n = r'\code{' + n.split('[')[0] +  r'}[' + n.split('[')[-1]
        plots.append(ptr.replace('|COORDS|', its).replace('|NAME|', n).replace('|I|', str(i+1)))
    name = r'\code{' + name.replace('_', r'\_') + r'}'
    return pattern.replace('|NAME|', name).replace('|ADDPLOTS|', '\n'.join(plots)).replace('|TICKS|', ','.join(df.columns))

    
def create_table(df, name):
    hcols = [r'\code{' + name + r'}']
    cols = [r'{}']
    for p in df.columns:
        cols.append('N'); cols.append('MAPE');
        hcols.append(r'\multicolumn{2}{c}{' + p + r'}')
    hcols = ' & '.join(hcols) + r'\\'
    cols = ' & '.join(cols) + r'\\'
    items = list()
    for i, ind in enumerate(df.index):
        s = ind + ' & '+ ' & '.join(df.values[i]) + r'\\'
        items.append(s)
    items = '\n'.join(items)
    table = r'\begin{tabular}{l|' + '|'.join(['cc'] * len(df.columns)) + r'}'
    table += '\n' + r'\toprule' + '\n' + hcols + '\n' + r'\midrule' + '\n' + cols + '\n' + items + '\n'
    table += r'\bottomrule' + '\n' + r'\end{tabular}'
    table = r'\begin{adjustbox}{width=\textwidth}' + '\n' + table + '\n'
    table += r'\end{adjustbox}'
    table = r'\begin{table}[H]' + '\n' + r'\centering' + '\n' + table
    table += '\n' + r'\caption{}' + '\n' + r'\end{table}'
    return table
        
        
        
    

params = os.listdir(folder_accuracy)

for param in params:
    path = os.path.join(folder_accuracy, param)
    files = [(f, os.path.join(path, f)) for f in os.listdir(path)
             if f.endswith('.pkl')]
    dall = dict()
    for name, p in files:
        with open(p, 'rb') as f:
            d = pickle.load(f)
        _, model, obj, solver = name[:-8].split('_')
        name = f'{model}[{obj}]'
        if solver != 'SLSQP':
            name += f'-{solver}'
        dall[name] = d
    ps = list(d.keys())
    if ps[0].isdecimal():
        t = list(map(float, ps))
        ps = sorted(ps, key=lambda x: t[ps.index(x)])
    else:
        ps = sorted(ps)
    if param == 'base':
        df_dict = dict()
    else:
        df_dict = defaultdict(list)
    for p in ps:
        succ = None
        for name, d in dall.items():
            items = d[p]
            mape = np.array(items[0])
            rmse = np.array(items[1])
            s = np.array(items[2])
            f = np.array(items[3])
            dt = np.array(items[4])
            s = (mape < 40) & s & np.isfinite(f)
            d[p] = (len(s) - sum(s), mape, rmse, dt)
            if succ is None:
                succ = s
            else:
                succ = succ & s
        for name, d in dall.items():
            n, mape, rmse, dt = d[p]
            mape = mape[succ].mean()
            rmse = rmse[succ].mean()
            dt = dt[succ].mean()
            if param == 'base':
                df_dict[name] = (n, mape, rmse, dt)
            else:
                df_dict[name].append(f'{n}&{mape}')
    if param == 'base':
        df = pd.DataFrame.from_dict(df_dict, orient='index',
                                    columns=['N', 'Mean MAPE', 'Mean RMSE', 
                                             'Time, s'])
    else:
        df = pd.DataFrame.from_dict(df_dict, orient='index',
                                    columns=ps)
    df.sort_index(inplace=True)
    df.sort_index(inplace=True, 
                  key=lambda x: [t.split('[')[-1] for t in x])
    for i in range(df.values.shape[0]):
        for j in range(df.values.shape[1]):
            tmp = str(df.values[i, j]).split('&')
            for k in range(len(tmp)):
                if k == 0:
                    pass
                else:
                    tmp[k] = '{:.2f}'.format(float(tmp[k]))
            t = '&'.join(tmp)
            if len(tmp) == 1:
                t = float(t)
            df.iloc[i, j] = t
    if param == 'base':
        try:
            df = df.drop(['Model[WLS]'])
        except KeyError:
            pass
        for col, ind in df.idxmin().iteritems():
            v = str(df.loc[ind, col])
            df.loc[ind, col] = r'\textbf{' + v + '}'
    path = os.path.join(folder_tables, f'{param}.csv')
    prepare_path(folder_tables)
    df.to_csv(path)
    if param == 'base':
        st = df.to_latex(escape=False)
        with open(os.path.join(folder_tables, f'{param}.tex'), 'w') as f:
            f.write(st)
    else:
        st = create_table(df, param)
        with open(os.path.join(folder_tables, f'{param}.tex'), 'w') as f:
            f.write(st)
        st = create_plot(df, param)
        with open(os.path.join(folder_tables, f'{param}_plot.tex'), 'w') as f:
            f.write(st)