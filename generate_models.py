#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from semopy import Model, ModelMeans, ModelEffects
from copy import deepcopy
import scipy
import numpy as np
import pandas as pd
import random
import os
import semopy
from utils import *

modgen = semopy.model_generation

num_models = 40 # Number of model structures
num_repeats = 10 # Number of parameter+data generations per structure
# In total num_models * num_repeats models.


def gen_norm(shape: tuple):
    return np.random.normal(size=shape)

def gen_beta(shape: tuple, a, b):
    return np.random.beta(a, b, size=shape)

def gen_int(shape: tuple):
    return np.random.choice([0, 1, 2], size=shape, p=[0.98, 0.01, 0.01])

def param_vars(mult=1):
    return mult * random.uniform(0.7, 1.3)


base_desc = {'n_endo': 3, 'n_exo': 4, 'n_lat': 2, 'n_inds': 3, 'n_cycles': 0,
             'p_join': 0.05, }
base_params = {'sampler_var_psi': param_vars}
base_data = {'n': 100, 'generator_exo': gen_norm}

def generate_models(path, key: str, vals: list, random_effects=False):
    ddesc = deepcopy(base_desc)
    ddata = deepcopy(base_data)
    dparams = deepcopy(base_params)
    for val in vals:
        if type(val) is tuple:
            valname, val = val
        else:
            valname = str(val)
        vpath = os.path.join(path, valname)
        if key is not None:
            if key in ddesc:
                ddesc[key] = val
            elif key in ddata:
                ddata[key] = val
            else:
                dparams[key] = val
        for i in range(num_models):
            random.seed(seed + i * num_models)
            np.random.seed(seed + i * num_models)
            spath = os.path.join(vpath, str(i))
            prepare_path(spath)
            desc = modgen.generate_desc(**ddesc)
            with open(os.path.join(spath, 'desc.txt'), 'w') as f:
                f.write(desc)
            for j in range(num_repeats):
                random.seed(seed + i * num_models + j)
                np.random.seed(seed + i * num_models + j)
                params, t = modgen.generate_parameters(desc, False,
                                                       **dparams)
                data = modgen.generate_data(t, **ddata)
                if random_effects:
                    n = len(data)
                    obs = [c for c in data.columns if not c.startswith('g')]
                    m = len(obs)
                    t = np.random.normal(size=(n, n))
                    t = t @ t.T + np.identity(n) * 0.05
                    t2 = np.identity(m)
                    u = scipy.stats.matrix_normal.rvs(rowcov=t, colcov=t2)
                    u -= u.mean(axis=0)
                    u /= u.std(axis=0)
                    data['group'] = list(map(lambda x: 'gr' + str(x),
                                             data.index))
                    k = pd.DataFrame(np.cov(u), index=data['group'],
                                     columns=data['group'])
                    k.to_csv(os.path.join(spath, f'{j}_k.csv'))
                    data[obs] += u
                params.to_csv(os.path.join(spath, f'{j}_params.csv'))
                data.to_csv(os.path.join(spath, f'{j}_data.csv'))

experiments = [('n_endo', [2, 3, 4, 5, 6, 7]),
               ('n_exo', [2, 3, 4, 5, 6, 7, 8]),
               ('n_lat', [1, 2, 3, 4, 5, 6]),
               ('n_cycles', [1, 2, 3, 4]),
               ('n', [50, 100, 150, 200, 250, 300, 350, 400]),
               ]


print('Generating base models...')
path = os.path.join(folder_accuracy, 'base')
generate_models(path, None, ['base'])
print('Generating random effect models with base configuration...')
path = os.path.join(folder_accuracy, 'rf')
generate_models(path, None, ['rf'], random_effects=True)
for name, vals in experiments:
    print(f'Generating models for parameter {name}...')
    path = os.path.join(folder_accuracy, name)
    generate_models(path, name, vals)


base_desc = {'n_endo': 1, 'n_exo': 3, 'n_lat': 1, 'n_inds': 3, 'n_cycles': 0,
              'p_join': 0.05, }
base_data = {'n': 100, 'generator_exo': gen_norm}
experiments = [('n', [20, 25, 30, 35, 40, 50,]),]
for name, vals in experiments:
    print(f'Generating PBS models for paremter {name}...')
    path = os.path.join(folder_accuracy_pbs, name)
    generate_models(path, name, vals)
    
base_desc = {'n_endo': 0, 'n_exo': 20, 'n_lat': 3, 'n_inds': 3, 'n_cycles': 0,
              'p_join': 0.05, }
base_data = {'n': 200, 'generator_exo': gen_norm}
experiments = [
                ('n_exo', [5, 10, 15, 20, 25, 30, 35, 40]),
                ('n_lat', [2, 3, 4, 5, 6,]),
                ('n', [50, 100, 150, 200, 250, 300, 350]),
                ]
print('--EFA generation--')
for name, vals in experiments:
    print(f'Generating EFA models for parameter {name}...')
    path = os.path.join(folder_efa, name)
    generate_models(path, name, vals)
    
    
base_desc = {'n_endo': 0, 'n_exo': 0, 'n_lat': 3, 'n_inds': 3, 'n_cycles': 0,
              'p_join': 0.05, }
base_data = {'n': 200, 'generator_exo': gen_norm}
experiments = [
                ('sampler_var_psi', [('0.5', lambda: param_vars(0.5)),
                                      ('1.0', lambda: param_vars(1)),
                                      ('2.0', lambda: param_vars(2)),
                                      ('4.0', lambda: param_vars(4)),
                                      ('8.0', lambda: param_vars(8)),
                                      ('16.0', lambda: param_vars(16)),
                                      ('32.0', lambda: param_vars(32)),
                                      ('64.0', lambda: param_vars(64))])
                ]
print('--EFA [sample_var_psi] generation--')
for name, vals in experiments:
    print(f'Generating EFA models for parameter {name}...')
    path = os.path.join(folder_efa, name)
    generate_models(path, name, vals)