#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from semopy import Model, ModelMeans, ModelEffects
from copy import deepcopy
from time import time
import scipy
import numpy as np
import pandas as pd
import os
import semopy
import pickle
import logging
from utils import *
logging.basicConfig(level=logging.ERROR)
np.seterr('ignore')

def evaluate_models(path: str, mod, obj: str,  solver='SLSQP'):
    errs_p = dict()
    res_name = f'result_{mod}_{obj}_{solver}.csv'
    for param in filter(lambda x: os.path.isdir(os.path.join(path, x)),
                        os.listdir(path)):
        errs_p[param] = [list(), list(), list(), list(), list()]
        models_path = os.path.join(path, param)
        errs_i = dict()
        for i in filter (lambda x: x.isdecimal(), os.listdir(models_path)):
            mod_path = os.path.join(models_path, i)
            with open(os.path.join(mod_path, 'desc.txt'), 'r') as f:
                desc = f.read()
            errs = dict()
            j = 1
            while os.path.isfile(os.path.join(mod_path, f'{j}_data.csv')):
                data = pd.read_csv(os.path.join(mod_path, f'{j}_data.csv'),
                                   index_col=0)
                params = pd.read_csv(os.path.join(mod_path, f'{j}_params.csv'),
                                     index_col=0)
                if mod == 'ModelEffects':
                    k = pd.read_csv(os.path.join(mod_path, f'{j}_k.csv'),
                                    index_col=0)
                try:
                    dt1 = time()
                    if mod == 'Model':
                        m = Model(desc)
                        r = m.fit(data, obj=obj, solver=solver)
                    elif mod == 'ModelMeans':
                        m = ModelMeans(desc)
                        r = m.fit(data, obj=obj, solver=solver)
                    elif mod == 'ModelEffects':
                        m = ModelEffects(desc)
                        r = m.fit(data, obj=obj, group='group', k=k,
                                  solver=solver)
                    dt2 = time()
                    mape = semopy.utils.compare_results(m, params)
                    mape = np.mean(mape) * 100
                    rmse = np.array(semopy.utils.compare_results(m, params,
                                                                 'abs'))
                    rmse = np.sqrt(rmse ** 2).mean()
                    if type(r) is tuple:
                        succ = r[0].success & r[1].success
                        fun = r[1].fun
                    else:
                        succ = r.success
                        fun = r.fun
                except np.linalg.LinAlgError:
                    mape = np.nan
                    rmse = mape
                    succ = False
                    fun = np.nan
                errs[j] = (mape, rmse, succ, fun)
                pt = errs_p[param]
                pt[0].append(mape); pt[1].append(rmse); pt[2].append(succ);
                pt[3].append(fun); pt[4].append(dt2-dt1)
                j += 1
            df = pd.DataFrame.from_dict(errs, columns=['MAPE', 'RMSE', 
                                                       'Opt. converged',
                                                       'Loss function'],
                                        orient='index')
            df.to_csv(os.path.join(mod_path, res_name))
            succs = df['Opt. converged'].values & (df['MAPE'] < 40) & \
                    np.isfinite(df['Loss function'])
            mean_mape = df[succs]['MAPE'].mean()
            median_mape = df[succs]['MAPE'].median()
            mean_rmse = df[succs]['RMSE'].mean()
            median_rmse = df[succs]['RMSE'].median()
            num_fail = len(df) - sum(succs)
            errs_i[i] = (num_fail, mean_mape, median_mape, mean_rmse,
                         median_rmse)
        df = pd.DataFrame.from_dict(errs_i, columns=['N fails', 'Mean MAPE', 
                                                     'Median MAPE',
                                                     'Mean RMSE',
                                                     'Median RMSE'],
                                    orient='index')
        df.to_csv(os.path.join(models_path, res_name))
    res_dict = dict()
    for param, (mape, rmse, succ, fun, dt) in errs_p.items():
        mape = np.array(mape); rmse = np.array(rmse)
        succ = (mape < 40) & np.array(succ) & np.isfinite(fun)
        mean_mape = np.mean(mape[succ]); median_mape = np.median(mape[succ])
        mean_rmse = np.mean(rmse[succ]); mean_rmse = np.median(rmse[succ])
        mean_fun = np.mean(np.array(fun)[succ]);
        median_fun = np.median(np.array(fun)[succ])
        mean_time = np.mean(np.array(dt)[succ])
        n_fails = len(mape) - sum(succ)
        res_dict[param] = (n_fails, mean_mape, median_mape, mean_rmse,
                           median_rmse, mean_fun, median_fun, mean_time)
    df = pd.DataFrame.from_dict(res_dict, columns=['N fails', 'Mean MAPE',
                                                   'Median MAPE', 'Mean RMSE',
                                                   'Median RMSE',
                                                   'Mean F', 'Median F',
                                                   'Mean time'],
                                orient='index')
    df.to_csv(os.path.join(path, res_name))
    with open(os.path.join(path, res_name + '.pkl'), 'wb') as f:
        pickle.dump(errs_p, f)
    
experiments = {
    'base': [
              ('Model', 'MLW', 'SLSQP'), ('Model', 'FIML', 'SLSQP'), 
              ('Model', 'GLS', 'SLSQP'), ('Model', 'WLS', 'SLSQP'),
              ('Model', 'DWLS', 'SLSQP'), ('Model', 'ULS', 'SLSQP'), 
              ('ModelMeans', 'REML', 'SLSQP'), ('ModelMeans', 'ML', 'SLSQP')],
    'n': [('Model', 'MLW', 'SLSQP'), ('Model', 'FIML', 'SLSQP'),
          ('ModelMeans', 'ML', 'SLSQP')],
    'n_exo': [('Model', 'MLW', 'SLSQP'), ('Model', 'FIML', 'SLSQP'),
              ('ModelMeans', 'ML', 'SLSQP'), ('ModelMeans', 'REML', 'SLSQP')],
    'n_endo': [('Model', 'MLW', 'SLSQP'), ('Model', 'FIML', 'SLSQP'),
                ('ModelMeans', 'ML', 'SLSQP')],
    'n_lat': [('Model', 'MLW', 'SLSQP'), ('Model', 'FIML', 'SLSQP'),
              ('ModelMeans', 'ML', 'SLSQP')],
    'n_cycles': [('Model', 'MLW', 'SLSQP'), ('Model', 'FIML', 'SLSQP'),
                  ('ModelMeans', 'ML', 'SLSQP')],
    'rf': [('Model', 'FIML', 'SLSQP'), ('ModelEffects', 'REML', 'SLSQP'),
            ('ModelEffects', 'ML', 'SLSQP')]
    }
print('---Evaluating models----')
for param, items in experiments.items():
    print(param)
    for mod, obj, solver in items:
        print(f'\t{mod} {obj} {solver}')
        evaluate_models(os.path.join(folder_accuracy, param), mod, obj,
                        solver)